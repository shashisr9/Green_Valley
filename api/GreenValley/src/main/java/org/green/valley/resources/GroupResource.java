package org.green.valley.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.green.valley.model.Group;
import org.green.valley.service.GroupService;

@Path("/groups")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GroupResource {

	private GroupService profileService = new GroupService();
	
	
	
	@GET
	public List<Group> getProfiles() {
		return profileService.getAllProfiles();
	}
	
	@POST
	public Group addProfile(Group profile) {
		return profileService.addProfile(profile);
	}
	
	@GET
	@Path("/{groupName}")
	public Group getProfile(@PathParam("profileName") String profileName) {
		return profileService.getProfile(profileName);
	}
	
	@PUT
	@Path("/{groupName}")
	public Group updateProfile(@PathParam("groupName") String groupName, Group profile) {
		profile.setProfileName(groupName);
		return profileService.updateProfile(profile);
	}
	
	
	
	
	
}
