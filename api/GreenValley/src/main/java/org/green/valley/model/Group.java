package org.green.valley.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class Group {

	private long id;
    private String groupName;
    private String firstName;
    private String lastName;
    private Date created;
    
    public Group() {
    	
    }
    
	public Group(long id, String profileName, String firstName, String lastName) {
		this.id = id;
		this.groupName = profileName;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProfileName() {
		return groupName;
	}
	public void setProfileName(String profileName) {
		this.groupName = profileName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
    
    
}
